__author__ = 'Khoir MS'

from auth import Auth
from endpoints import Endpoints
from ratelimit import rate_limited
from requests.exceptions import HTTPError

class Instagram(Auth):
    __MAX_LOGIN_ATTEMPTS = 1

    def __init__(self, **kwargs):
        super(Instagram, self).__init__(**kwargs)
        self.response = None
        self.qid_posts = '17880160963012870'
        self.qid_hashtag = '17882293912014529'
        self.qid_location = '17881432870018455'
        self.qid_comment = '17852405266163336'
        self.qid_profile = None

    def prepare(self, username=None, password=None, cookies=None):
        if not username: raise Exception("username is not defined")
        if not password: raise Exception("password is not defined")

        self.username = username
        self.password = password
        self.cookies = cookies

        try:
            attempts = 0
            while not self.login_status and attempts < self.__MAX_LOGIN_ATTEMPTS:
                self.authentication()
                attempts += 1
        except:
            self.log(level='error')

        if not self.login_status:
            self.log('only using instagram header', level='warning')
            self.set_header()

    def search(self, query, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        endpoint = Endpoints.getGeneralSearchJsonLink(query=query)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.raise_api_error()

        return self.response.json()

    def profile(self, username, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.change_user_agent()
        self.change_referer(referer='https://www.instagram.com/{}/'.format(username))

        self.retry_session()
        self.set_proxies(proxies)
        endpoint = Endpoints.getAccountJsonLink(username=username)
        self.response = self.session.get(url=endpoint, timeout=timeout)

        # checking error
        self.raise_api_error(AccountNotExists)

        return self.response.json()

    def media(self, user_id, max_result=12, cursor='', query_id='', **kwargs):
        if query_id:
            self.qid_posts = query_id
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.change_user_agent()

        endpoint = Endpoints.getMediaByUserID(query_id=self.qid_posts, user_id=user_id, count=max_result, cursor=cursor)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.response.raise_for_status()
        self.raise_api_error(AccountNotExists)

        return self.response.json()

    def media_by_tag(self, tag, max_result=12, query_id='', cursor='', **kwargs):
        if query_id:
            self.qid_hashtag = query_id
        tag = tag.lower().replace(' ', '')
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.change_user_agent()
        self.change_referer(referer='https://www.instagram.com/explore/tags/{}/'.format(tag))

        endpoint = Endpoints.getMediaByHashTag(query_id=self.qid_hashtag, hashtag=tag, count=max_result, cursor=cursor)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.raise_api_error()

        return self.response.json()

    def media_by_location(self, location_id, max_result=12, query_id='', cursor='', **kwargs):
        if query_id:
            self.qid_location = query_id
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.change_user_agent()
        self.change_referer(referer='https://www.instagram.com/explore/locations/{}/'.format(location_id))

        endpoint = Endpoints.getMediaByLocation(query_id=self.qid_location, location_id=location_id, count=max_result, cursor=cursor)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.raise_api_error()

        return self.response.json()

    def media_detail(self, code, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.change_user_agent()

        endpoint = Endpoints.getMediaJsonLink(code=code)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.raise_api_error(MediaNotExists)

        return self.response.json()

    @rate_limited(20, 20.0)
    def comments(self, code, max_result=1000, cursor='', query_id='', **kwargs):
        if query_id:
            self.qid_comment = query_id
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        # self.change_user_agent()
        self.change_referer(referer='https://www.instagram.com/p/{}/'.format(code))

        endpoint = Endpoints.getCommentsByCodeLink(query_id=self.qid_comment, code=code, count=max_result, cursor=cursor)
        self.retry_session()
        self.set_proxies(proxies)
        self.response = self.session.get(url=endpoint, timeout=timeout)
        # checking error
        self.raise_api_error(MediaNotExists)
        data = self.response.json()

        if len(data) == 1 and "status" in data:
            raise MediaNotExists("this page https://www.instagram.com/p/{} isn't available".format(code))

        return data

    def raise_api_error(self, exception=None):
        """Raises stored :class:`HTTPError`, if one occurred."""

        http_error_msg = ''
        if isinstance(self.response.reason, bytes):
            # We attempt to decode utf-8 first because some servers
            # choose to localize their reason strings. If the string
            # isn't utf-8, we fall back to iso-8859-1 for all other
            # encodings. (See PR #3538)
            try:
                reason = self.response.reason.decode('utf-8')
            except UnicodeDecodeError:
                reason = self.response.reason.decode('iso-8859-1')
        else:
            reason = self.response.reason

        if 400 <= self.response.status_code < 500:
            http_error_msg = u'%s Client Error: %s for url: %s' % (self.response.status_code, reason, self.response.url)

        elif 500 <= self.response.status_code < 600:
            http_error_msg = u'%s Server Error: %s for url: %s' % (self.response.status_code, reason, self.response.url)

        if http_error_msg:
            if self.response.status_code == 404 and exception:
                raise exception(http_error_msg)
            if self.response.status_code == 429:
                raise TooManyRequest(http_error_msg)
            raise HTTPError(http_error_msg, response=self.response)

class AccountNotExists(Exception):
    pass

class MediaNotExists(Exception):
    pass

class TooManyRequest(Exception):
    pass

if __name__== '__main__':
    import json
    # example access
    ig = Instagram(debug=True)
    ig.set_header()
    # ig.prepare(username='clipper.dacol', password='Rahasia123456')

    # data = ig.search(query='bandung')
    # data = ig.profile(user_id='40528624', proxies={'http': 'http://103.75.25.100:3128'})
    # data = ig.profile(username='ridwankamil')
    # data = ig.media(username='ridwankamil')
    # data = ig.media_by_tag(tag='rajasalman')
    # data = ig.media_by_location(location_id=108988619122058)
    # data = ig.media_detail(code='BSc3IUpFiUR')
    # data = ig.comments(code='BSw9o19j6Z_')
    # print json.dumps(data, indent=4)
    try:
        data = ig.media(user_id='40528624')
        # data = ig.media_by_tag(tag='macet')
        # data = ig.media_by_location(location_id=108988619122058)
        # data = ig.comments(code='BVPfJwTDObh', max_result=2, timeout=10, proxies={'http': 'http://103.75.25.102:3128'})
        # data = ig.comments(code='BVPfJwTDObh', max_result=2, timeout=10)
        # data = ig.profile(username='ridwankamil')
        print json.dumps(data, indent=4)
    except (AccountNotExists,MediaNotExists) as e:
        print repr(e)
    # except HTTPError as e:
    #     print ig.response.status_code