__author__ = 'Khoir MS'

import time
import random
from browser import Browser
from endpoints import Endpoints

class Auth(Browser):

    def __init__(self, **kwargs):
        super(Auth, self).__init__(**kwargs)

        self.baseurl = Endpoints.baseUrl()
        self.login_url = Endpoints.login()

        self.username = None
        self.password = None
        self.login_status = False

    def authentication(self):
        try:
            if self.cookies:
                self.log('login with cookies')
                if self.set_cookies():
                    self.__is_logged_in()
                    if not self.login_status:
                        self.log('failed login with cookies')
                        self.login()
                else:
                    self.log('failed login with cookies')
                    self.login()
            else:
                self.login()

            if self.login_status:
                self.log('logged in, robot is ready to use')
            else:
                raise Exception('sorry, robot is not ready to use')
        except:
            raise

    def set_header(self):
        self.log('setuping request header')
        self.session.cookies.update(self.cookie_set())

        self.session.headers.update({
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': self.accept_language(),
            'Connection': 'keep-alive',
            'Content-Length': '0',
            'Host': 'www.instagram.com',
            'Origin': 'https://www.instagram.com',
            'Referer': 'https://www.instagram.com/',
            'User-Agent': self.user_agents(),
            'X-Instagram-AJAX': '1',
            'X-Requested-With': 'XMLHttpRequest'
        })

        r = self.session.get(self.baseurl)
        self.session.headers.update({'X-CSRFToken': r.cookies['csrftoken']})

    def login(self, username=None, password=None):
        self.log('login with username and password')

        if username:
            self.username = username
        if password:
            self.password = password

        if not self.username or not self.password:
            raise Exception('username or password not defined')

        self.set_header()
        time.sleep(5 * random.random())
        login = self.session.post(self.login_url, data={'username': self.username, 'password': self.password}, allow_redirects=True)
        self.session.headers.update({'X-CSRFToken': login.cookies['csrftoken']})
        self.csrftoken = login.cookies['csrftoken']
        time.sleep(5 * random.random())

        if login.status_code == 200:
            self.__is_logged_in()
        else:
            self.log('login error, connection error !')

    def __is_logged_in(self):
        r = self.session.get(Endpoints.baseUrl())
        finder = r.text.find(self.username)

        if finder != -1:
            self.login_status = True
            self.log('look like login by {} success !'.format(self.username))
        else:
            self.login_status = False
            self.log('login error, check your login data !')

    def __logout(self):
        pass

if __name__=="__main__":
    import requests
    # import cookielib
    # cookielib.Cookie()
    #
    # cj = cookielib.LWPCookieJar(cookie_file)

    A = Auth()
    # auth = A.session.get(url='http://192.168.20.160:9020/get_user_instagram')
    # auth = auth.json()
    # A.username = auth['data']['screen_name']
    # A.password = auth['data']['password']
    # A.cookies = pickle.loads(auth['data']['cookie'])
    # print A.cookies
    # A.authentication()
    A.set_header()
    payload = Endpoints.parseUrlParameters(Endpoints.getLastMediasJsonByTag('rajasalman'))
    print payload

    r = A.session.post_detail(url=Endpoints.baseQueryUrl(), data=payload)
    print r.json()