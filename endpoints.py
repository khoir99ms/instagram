#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Muhammad Sopan H'

import urlparse

class Endpoints:
    
    __BASE_URL = 'https://www.instagram.com'
    __GRAPHQL = 'https://www.instagram.com/graphql/query/?query_id={query_id}'

    __ACCOUNT_INFO = 'https://www.instagram.com/{username}/?__a=1'
    __GENERAL_SEARCH = 'https://www.instagram.com/web/search/topsearch/?query={query}'
    __MEDIA_JSON_INFO = 'https://www.instagram.com/p/{code}/?__a=1'

    __MEDIA_BY_USER_ID = '{graphql}&id={user_id}&first={count}&after={cursor}'
    __MEDIA_BY_HASHTAG = '{graphql}&tag_name={hashtag}&first={count}&after={cursor}'
    __MEDIA_BY_LOCATION = '{graphql}&id={location_id}&first={count}&after={cursor}'
    __COMMENTS_BY_CODE = '{graphql}&shortcode={code}&first={count}&after={cursor}'

    __LOGIN_URL = 'https://www.instagram.com/accounts/login/ajax/'
    __LOGOUT_URL = 'https://www.instagram.com/accounts/logout/'

    @staticmethod
    def parseUrlParameters(url):
        return dict(urlparse.parse_qsl(urlparse.urlsplit(url).query))

    @staticmethod
    def baseUrl():
        return Endpoints.__BASE_URL

    @staticmethod
    def getGeneralSearchJsonLink(query):
        return Endpoints.__GENERAL_SEARCH.replace('{query}', query)

    @staticmethod
    def getAccountJsonLink(username):
        return Endpoints.__ACCOUNT_INFO.format(username=username)

    @staticmethod
    def getMediaByUserID(query_id, user_id, count, cursor=''):
        graphql = Endpoints.__GRAPHQL.format(query_id=query_id)
        return Endpoints.__MEDIA_BY_USER_ID.format(graphql=graphql, user_id=user_id, count=count, cursor=cursor)

    @staticmethod
    def getMediaByHashTag(query_id, hashtag, count, cursor=''):
        graphql = Endpoints.__GRAPHQL.format(query_id=query_id)
        return Endpoints.__MEDIA_BY_HASHTAG.format(graphql=graphql, hashtag=hashtag, count=count, cursor=cursor)

    @staticmethod
    def getMediaByLocation(query_id, location_id, count, cursor=''):
        graphql = Endpoints.__GRAPHQL.format(query_id=query_id)
        return Endpoints.__MEDIA_BY_LOCATION.format(graphql=graphql, location_id=location_id, count=count, cursor=cursor)

    @staticmethod
    def getMediaJsonLink(code):
        return Endpoints.__MEDIA_JSON_INFO.replace('{code}', code)

    @staticmethod
    def getCommentsByCodeLink(query_id, code, count, cursor=''):
        graphql = Endpoints.__GRAPHQL.format(query_id=query_id)
        return Endpoints.__COMMENTS_BY_CODE.format(graphql=graphql, code=code, count=count, cursor=cursor)

    @staticmethod
    def login():
        return Endpoints.__LOGIN_URL

    @staticmethod
    def logout():
        return Endpoints.__LOGOUT_URL

if __name__=="__main__":
    pass