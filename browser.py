import re
import pickle
import random
import logging
import httplib
import requests
import dateparser

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from utils import datetime_to_timestamp
from logger import Logger

class Browser(Logger, object):
    __UAGENT = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/55.0.2883.87 Chrome/55.0.2883.87 Safari/537.36',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0',
        'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41',
        'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1',
        'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)',
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    ]
    __ACCEPT_LANGUAGE = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
    __COOKIE_SET = {'sessionid': '', 'mid': '', 'ig_pr': '1', 'ig_vw': '1920', 'csrftoken': '', 's_network': '', 'ds_user_id': ''}

    def __init__(self, **kwargs):
        super(Browser, self).__init__(logname=kwargs.get('logname'))

        self.cookies = None

        if kwargs.get('debug'):
            httplib.HTTPConnection.debuglevel = 1
            # logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s][%(name)s] - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
            logging.basicConfig()
            logging.getLogger().setLevel(logging.DEBUG)
            request_log = logging.getLogger('requests.packages.urllib3')
            request_log.setLevel(logging.DEBUG)
            request_log.propagate = True
        self.session = requests.Session()

        self.log('initialize ... ')

    def user_agents(self):
        return random.choice(self.__UAGENT)

    def change_user_agent(self):
        uagent = self.user_agents()
        self.log('change user agent to {}'.format(uagent))
        self.session.headers.update({'User-Agent': uagent})

    def change_referer(self, referer):
        self.log('change referer header to {}'.format(referer))
        self.session.headers.update({'Referer': referer})

    def accept_language(self):
        return self.__ACCEPT_LANGUAGE

    def cookie_set(self):
        return self.__COOKIE_SET

    def get_cookies(self):
        return pickle.dumps(self.session.cookies._cookies)

    def set_cookies(self, cookies=None):
        if cookies:
            self.cookies = cookies

        if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
            self.cookies = pickle.loads(self.cookies)

        try:
            self.verify_cookie()
            if isinstance(self.cookies, list):
                for cookie in self.cookies:
                    try:
                        self.session.cookies.set(**cookie)
                    except:
                        raise
            return True
        except:
            return False

    def verify_cookie(self):
        cookiejar = ['version', 'name', 'value', 'port', 'domain', 'path', 'secure', 'expires', 'discard', 'comment', 'comment_url']
        if isinstance(self.cookies, dict):
            self.cookies = [self.cookies]

        cookies = list()
        for cookie in self.cookies:
            allowed_cookie = {}
            for k,v in cookie.iteritems():
                if k in cookiejar:
                    if k == 'expires':
                        v = re.sub(r'^\w+,\s?', '', v.strip(), flags=re.U)
                        v = dateparser.parse(v)
                        if v: v = datetime_to_timestamp(v)
                    allowed_cookie[k] = v
            cookies.append(allowed_cookie)
        self.cookies = cookies

    def set_proxies(self, proxies=None):
        if proxies and isinstance(proxies, dict):
            self.session.proxies.update(proxies)

    def retry_session(self, retries=2, backoff_factor=0.1, status_forcelist=(500, 502, 504), session=None):
        self.session = session if session else self.session
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        self.session.mount('http://', adapter)
        self.session.mount('https://', adapter)